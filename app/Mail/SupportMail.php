<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SupportMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var Request
     */
    public $mail;

    /**
     * Create a new message instance.
     *
     * @param $ticket
     */
    public function __construct(Request $ticket)
    {
        $this->mail = $ticket;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $ticket = $this->mail->has('ticket') ? $this->mail->get('ticket') : [];
        $ticket_id = isset($ticket['id']) ? $ticket['id'] : 'Sin ID';
        $subject = isset($ticket['subject']) ? $ticket['subject'] : 'Incidencia Plataforma G Suite';
        $message = isset($ticket['message']) ? $ticket['message'] : 'Incidencia sin descripción';

        $requester = $this->mail->has('requester') ? $this->mail->get('requester') : [];
        $requester_name  = isset($requester['name']) ? $requester['name'] : 'Sin Nombre';
        $requester_email = isset($requester['email']) ? $requester['email'] : 'no-reply@idrd.gov.co';

        if ( ! filter_var($requester_email, FILTER_VALIDATE_EMAIL) ) {
            $requester_email = 'no-reply@idrd.gov.co';
        }

        $time = $this->mail->has('time') ? $this->mail->get('time') : Carbon::now()->format('Y-m-d H:i:s');

        $property = $this->mail->has('property') ? $this->mail->get('property') : [];
        $property_name = isset($property['name']) ? $property['name'] : 'Sin Nombre';

        return $this->view('mail.mail')
                    ->from($requester_email, $requester_name)
                    ->subject($subject)
                    ->with([
                        'id'    => $ticket_id,
                        'name'  =>  $requester_name,
                        'email' =>  $requester_email,
                        'time'  => $time,
                        'info'  => $message,
                        'property' => $property_name,
                        'url'   => 'https://dashboard.tawk.to/#/messaging/5ed94d024a7c62581799f11b',
                        'date'  => Carbon::now()->year
                    ]);
    }
}
