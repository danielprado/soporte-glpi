<?php

namespace App\Http\Controllers;

use App\Mail\SupportMail;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class GlpiController extends Controller
{
    const WEBHOOK_SECRET = '51ba81b421fbca861573bd0762d6eb3e133c4838d51c15b045daad6932df9c74b561f87ec469fef4575d9db9b10700f6';

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        //$flag = $this->verifySignature(file_get_contents('php://input'), $request->server('HTTP_X_TAWK_SIGNATURE'));
        Mail::to('soporte@idrd.gov.co')->send(new SupportMail($request));
        return response()->json(['data' => 'OK'], 200);
    }

    function verifySignature ($body, $signature) {
        $digest = hash_hmac('sha1', $body, self::WEBHOOK_SECRET);
        return $signature !== $digest ;
    }
}
